GMBuddy Web
=============================================

## Install Instructions

Install tsd: `npm install -g tsd`

Install typescript dependencies: `tsd install`

Install node dependencies: `npm install`


## Running Application

Start the server: `npm start`

Visit `localhost:3000` to view the application.
